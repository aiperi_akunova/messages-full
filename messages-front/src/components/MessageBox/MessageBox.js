import React from 'react';
import './MessageBox.css';

const MessageBox = props => {
    return (
        <div className='box'>
            <h3>Author: {props.title}</h3>
            <p> <b>Message: </b>{props.message}</p>
        </div>
    );
};

export default MessageBox;