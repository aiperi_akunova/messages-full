import axios from "axios";

export const FETCH_MESSAGES_REQUEST = 'FETCH_MESSAGES_REQUEST';
export const FETCH_MESSAGES_SUCCESS = 'FETCH_MESSAGES_SUCCESS';
export const FETCH_MESSAGES_FAILURE = 'FETCH_MESSAGES_FAILURE';

export const FETCH_LAST_MESSAGES_REQUEST = 'FETCH_LAST_MESSAGES_REQUEST';
export const FETCH_LAST_MESSAGES_SUCCESS = 'FETCH_LAST_MESSAGES_SUCCESS';
export const FETCH_LAST_MESSAGES_FAILURE = 'FETCH_LAST_MESSAGES_FAILURE';

export const CREATE_MESSAGE_REQUEST = 'CREATE_MESSAGE_REQUEST';
export const CREATE_MESSAGE_SUCCESS = 'CREATE_MESSAGE_SUCCESS';
export const CREATE_MESSAGE_FAILURE = 'CREATE_MESSAGE_FAILURE';

export const LAST_DATETIME ='LAST_DATETIME';

export const fetchMessagesRequest = () => ({type: FETCH_MESSAGES_REQUEST});
export const fetchMessagesSuccess = messages => ({type: FETCH_MESSAGES_SUCCESS, payload: messages});
export const fetchMessagesFailure = () => ({type: FETCH_MESSAGES_FAILURE});

export const fetchLastMessageRequest = () => ({type: FETCH_LAST_MESSAGES_REQUEST});
export const fetchLastMessageSuccess = messages => ({type: FETCH_LAST_MESSAGES_SUCCESS, payload: messages});
export const fetchLastMessageFailure = () => ({type: FETCH_LAST_MESSAGES_FAILURE});

export const createMessageRequest = () => ({type: CREATE_MESSAGE_REQUEST});
export const createMessageSuccess = () => ({type: CREATE_MESSAGE_SUCCESS});
export const createMessageFailure = () => ({type: CREATE_MESSAGE_FAILURE});

export const lastDatetime = date =>({type: LAST_DATETIME, payload: date});

export const fetchMessages = () => {
  return async dispatch => {
    try {
      dispatch(fetchMessagesRequest());
      const response = await axios.get('http://localhost:8000/messages');
      dispatch(fetchMessagesSuccess(response.data));
    } catch (e) {
      dispatch(fetchMessagesFailure());
    }
  };
};

export const fetchLastMessages = datetime => {
  return async dispatch => {
    try {
      dispatch(fetchLastMessageRequest());
      const response = await axios.get('http://localhost:8000/messages?datetime='+datetime);
        dispatch(fetchLastMessageSuccess(response.data));
    } catch (e) {
      dispatch(fetchLastMessageFailure());
    }
  };
};

export const createMessage = message => {
  return async dispatch => {
    try {
      dispatch(createMessageRequest());

      await axios.post('http://localhost:8000/messages', message);

      dispatch(createMessageSuccess());
    } catch (e) {
      dispatch(createMessageFailure());
      throw e;
    }
  };
};
