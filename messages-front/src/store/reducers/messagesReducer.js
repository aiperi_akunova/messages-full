import {
  FETCH_LAST_MESSAGES_FAILURE,
  FETCH_LAST_MESSAGES_REQUEST,
  FETCH_LAST_MESSAGES_SUCCESS,
  FETCH_MESSAGES_FAILURE,
  FETCH_MESSAGES_REQUEST,
  FETCH_MESSAGES_SUCCESS, LAST_DATETIME
} from "../actions/messagesActions";

const initialState = {
  fetchLoading: false,
  singleLoading: false,
  messages: [],
  newMessages:[],
  lastMessage: null,
};

const messagesReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_MESSAGES_REQUEST:
      return {...state, fetchLoading: true};
    case FETCH_MESSAGES_SUCCESS:
      return {...state, fetchLoading: false, messages: action.payload};
    case FETCH_MESSAGES_FAILURE:
      return {...state, fetchLoading: false};
    case FETCH_LAST_MESSAGES_REQUEST:
      return {...state, singleLoading: true};
    case FETCH_LAST_MESSAGES_SUCCESS:
      return {...state, singleLoading: false, newMessages: action.payload};
    case FETCH_LAST_MESSAGES_FAILURE:
      return {...state, singleLoading: false};
    case LAST_DATETIME:
      return {...state, lastMessage: action.payload}
    default:
      return state;
  }
};

export default messagesReducer;