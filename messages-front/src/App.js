import './App.css';
import {useEffect, useState} from "react";
import MessageBox from "./components/MessageBox/MessageBox";
import {useDispatch, useSelector} from "react-redux";
import {createMessage, fetchMessages, lastDatetime} from "./store/actions/messagesActions";

const App = () => {

    const dispatch = useDispatch();
    const messages = useSelector(state => state.messages.messages);
    // const lastMessage = useSelector(state => state.messages.lastMessage);
    // const newMessages = useSelector(state => state.messages.newMessages);
    const [userMessage, setUserMessage] = useState({text: '', author: ''});


    useEffect(() => {
        dispatch(fetchMessages());
    }, [dispatch])

    const index = messages.length-1;

    useEffect(()=>{
        dispatch(lastDatetime(messages[index]))
        const interval = setInterval(() => {
            // dispatch(fetchLastMessages(lastMessage.datetime));
        }, 3000);
        return ()=> clearInterval(interval);
    },[dispatch,index])


    const onPost = async message => {
        await dispatch(createMessage(message));
        setUserMessage({text: '', author: ''});
    };

    const onInputChange = e => {
        const {name, value} = e.target;
        setUserMessage(prev => ({
            ...prev,
            [name]: value
        }));
    };

    return (
        <div className="App">
            <h1>Welcome to JS-10 chat!</h1>
            <div className='input'>
                <label>
                    Message:
                    <input
                        autoComplete='off'
                        type='text'
                        value={userMessage.text}
                        name='text'
                        onChange={onInputChange}/>
                </label>

                <label>
                    Author:
                    <input
                        type='text'
                        autoComplete='off'
                        value={userMessage.author}
                        name='author'
                        onChange={onInputChange}/>
                </label>
                <button onClick={() => onPost(userMessage)}>Post</button>
            </div>
            <div className='main-box'>
                {messages.map(post =>
                    <MessageBox
                        title={post.author}
                        message={post.text}
                        key={post.id}
                    />
                )}
            </div>
        </div>
    );
};

export default App;
