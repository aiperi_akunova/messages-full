const express = require('express');
const fileDb = require('../fileDb');
const router = express.Router();

router.get('/', (req, res) => {
const dateTime = req.query.datetime;
  const messages = fileDb.getItems();

if(dateTime){
  const date = new Date(req.query.datetime);
  if (isNaN(date.getDate())){
    res.send({error: "Date is not valid"})
  } else {

    const results = messages.filter(message => {
      return (message.datetime > dateTime);
    });

    res.send(results);
  }
}
  res.send(messages);
});

router.post('/', (req, res) => {
  if (!req.body.author || !req.body.text) {
    return res.status(400).send({error: 'Author and message must be present in the request'});
  }

  const newMessage = fileDb.addItem({
    author: req.body.author,
    text: req.body.text,
  });

  res.send(newMessage);
});

module.exports = router; // export default router;